Rails.application.routes.draw do
  get 'posts/welcome'

  get 'abouts/about'

  root 'posts#index'
  resources :posts

  resources :artista
  devise_for :usuarios

  
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
