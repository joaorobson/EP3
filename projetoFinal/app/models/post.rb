class Post < ApplicationRecord
	
	
	has_attached_file :image, :styles => {:medium => "500*500"}
	validates_attachment_presence :image
	validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
	has_attached_file :music
	validates_attachment_presence :music
	validates_attachment_content_type :music, :content_type => /.*/

end

