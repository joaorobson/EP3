class AddAttachmentMusicImageToPosts < ActiveRecord::Migration
  def self.up
    change_table :posts do |t|
      t.attachment :music
      t.attachment :image
    end
  end

  def self.down
    remove_attachment :posts, :music
    remove_attachment :posts, :image
  end
end
